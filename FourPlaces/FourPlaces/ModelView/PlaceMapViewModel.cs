﻿using Common.Api.Dtos;
using FourPlaces.RestApiServices;
using FourPlaces.Views.PlaceMapPage;
using Storm.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class PlaceMapViewModel : ViewModelBase
    {
        private PlaceItemSummary item;
        public ObservableCollection<CommentItem> PlacesComments { get; set; }
        
        private readonly INavigation _navigation;
        private PlaceMap page;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private string _comment;
        public string Comment
        {
            get => _comment;
            set => SetProperty(ref _comment, value);
        }

        private bool _displayCommentView;
        public bool DisplayCommentView
        {
            get => _displayCommentView;
            set => SetProperty(ref _displayCommentView, value);
        }

        private bool _displayCommentButton;
        public bool DisplayCommentButton
        {
            get => _displayCommentButton;
            set => SetProperty(ref _displayCommentButton, value);
        }
        
        public string ImageUrl
        {
            get => item.ImageUrl;
        }

        public string Title
        {
            get => item.Title;
        }

        public string Description
        {
            get => item.Description;
        }

        public string DistanceFromUser
        {
            get => DistanceString(); 
        }

        private string DistanceString()
        {
            var meters = (Int32)Math.Round(1000 * item.DistanceFromUser, 0);
            if(meters < 1000)
                return meters + " m de vous";
            else return Math.Round(item.DistanceFromUser, 0) + " km de vous";

        }

        public ICommand BtnPopupButton_Comment { get; }
        public ICommand BtnPopupButton_Clicked { get; }

        public PlaceMapViewModel(PlaceMap placeMap,PlaceItemSummary item)
        {
            _navigation = placeMap.Navigation;
            this.item = item;
            page = placeMap;
            this.PlacesComments = new ObservableCollection<CommentItem>();

            DisplayCommentButton = true;
            DisplayCommentView = false;

            BtnPopupButton_Comment = new Command(GoCommentButtonAction);
            BtnPopupButton_Clicked = new Command(GoDisplayPopUpAction);
        }

        private void GoDisplayPopUpAction(object obj)
        {
            DisplayCommentView = true;
            DisplayCommentButton = false;
        }

        private async void GoCommentButtonAction(object obj)
        {
            DisplayCommentView = false;
            DisplayCommentButton = true;

            if (Comment != null && Comment.Length >= 0)
            {
                CreateCommentRequest requestComment = new CreateCommentRequest
                {
                    Text = Comment
                };
                IsBusy = true;
                var res = await App.Rest_services.PostResponse<Response>(Constants.PlacesUrl + "/" + item.Id + "/comments", requestComment);

                // l'utilisateur n'est pas conecté
                if (res == null && !App.Rest_services.IsConnected())
                {
                    App.DisplayMessageService.InternetConnectionNeeded(page);
                }
                else
                {
                    if (res != null && res.IsSuccess)
                    {
                        App.DisplayMessageService.DisplayMessage(page, "Succès", "Commentaire posté avec succès", "OK");
                        RefreshComment();
                        IsBusy = false;
                    }
                    else
                    {
                        var msg = "Impossible d'enregistrer votre commentaire.\n";
                        if (res != null && res.ErrorMessage != null)
                        {
                            msg += res.ErrorMessage;
                        }
                        App.DisplayMessageService.DisplayMessage(page,"Erreur",msg , "OK");
                        IsBusy = false;
                    }
                }
            }
            else App.DisplayMessageService.DisplayEmptyFormError(page);
        }
        

        public override async Task OnResume()
        {
            await base.OnResume();
            RefreshComment();
        }

        // Permet de mettre à jour les commentaires de l'item
        private async void RefreshComment()
        {
            IsBusy = true;
            var res = await App.Rest_services.GetResponse<PlaceItem>(Constants.PlacesUrl + "/" + item.Id);
            if (res == null && !App.Rest_services.IsConnected())
            {
                App.DisplayMessageService.InternetConnectionNeeded(page);
            }
            else
            {
                if (res == null || res.Data == null || res.IsSuccess == false || res.Data.Comments == null)
                {
                    string msg = "Impossible charger les commentaires.\n";
                    if (res != null && res.ErrorMessage != null)
                    {
                        msg += res.ErrorMessage;
                    }
                    App.DisplayMessageService.DisplayMessage(page, "Alert", msg, "OK");
                }
                else
                {
                    PlacesComments.Clear();
                    foreach (CommentItem elt in res.Data.Comments)
                    {
                        PlacesComments.Add(elt);
                    }
                }
            }
            IsBusy = false;
        }
    }

        

        
}
