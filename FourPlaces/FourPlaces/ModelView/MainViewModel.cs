﻿using FourPlaces.Pages.AuthenticationPage;
using FourPlaces.Pages.ProfilePage;
using FourPlaces.Pages.RegistrationPage;
using FourPlaces.Views.NewPlacePage;
using FourPlaces.Views.PlaceItemPage;
using MonkeyCache.FileStore;
using Storm.Mvvm;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class MainViewModel : ViewModelBase
    {
        // _navigation permet de naviguer entre les différentes pages de l'appli
        private INavigation _navigation;
        private readonly MainPage _mainPage;

        private string _homePage;
        public string HomePage
        {
            get => _homePage;
            set => SetProperty(ref _homePage, value);
        }

        private bool _userLogged;
        public bool UserLogged
        {
            get => _userLogged;
            set => SetProperty(ref _userLogged, value);
        }

        private bool _userNotLogged;
        public bool UserNotLogged
        {
            get => _userNotLogged;
            set => SetProperty(ref _userNotLogged, value);
        }

        private string _userName;
        public string UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }


        public ICommand GoRegistrationCommand
        {
            get;
        }

        public ICommand GoMapCommand
        {
           get;
        }

        public ICommand GoPlacesItemCommand
        {
            get;
        }

        public ICommand GoSignInCommand
        {
            get;
        }

        public ICommand GoProfileCommand
        {
            get;
        }

        public ICommand GoAddItemCommand
        {
            get;
        }

        public ICommand GoDecoCommand { get; }



        public MainViewModel(MainPage mainPage, INavigation navigation)
        {
            _navigation = navigation;
            this._mainPage = mainPage;

            UserLogged = false;
            UserNotLogged = true;

            GoRegistrationCommand = new Command(GoRegisterAction);
            GoMapCommand = new Command(GoMapAction);
            GoPlacesItemCommand = new Command(GoPlacesItemAction);
            GoSignInCommand = new Command(GoSignInAction);
            GoProfileCommand = new Command(GoProfileAction);
            GoAddItemCommand = new Command(GoAddItemAction);
            GoDecoCommand = new Command(GoDecoAction);
        }

        private void GoDecoAction()
        {
            // on supprime le cache
            Barrel.Current.EmptyAll();
            App.UserInfo = null;
            UserName = "";
            HomePage = "Identification";
            UserNotLogged = true;
            UserLogged = false;
        }

        private async void GoAddItemAction(object obj)
        {
            App.DisplayMessageService.DisplayEnableGeolocation(_mainPage);
            await _navigation.PushAsync(new NewPlace());
        }

        private async void GoProfileAction(object obj)
        {
            await _navigation.PushAsync(new Profile());
        }
        private async void GoSignInAction(object obj)
        {
            await _navigation.PushAsync(new Authentication());
        }

        private async void GoPlacesItemAction(object obj)
        {
            App.DisplayMessageService.DisplayEnableGeolocation(_mainPage);
            await _navigation.PushAsync(new PlaceItem());
        }

        private async void GoMapAction(object obj)
        {
            
            await _navigation.PushAsync(new Pages.MapPage.Map());
        }

        private async void GoRegisterAction(object obj)
        {
            await _navigation.PushAsync(new Registration());
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            // l'utilisateur n'est pas conecté
            if (!App.Rest_services.IsConnected())
            {
                App.DisplayMessageService.InternetConnectionNotActivated(_mainPage);
            }
            if (App.tokenInfo != null)
            {
                // lorsque le user est connecté on affiche le menu principal
                UserNotLogged = false;
                UserLogged = true;
                HomePage = "Menu principal";

                if(App.UserInfo != null)
                {
                    UserName = "Compte : " + App.UserInfo.Email + ".\n Utilisateur : " + App.UserInfo.FirstName;
                }

                if (Barrel.Current != null && !Barrel.Current.IsExpired(key: App.CacheUserInfoKey))
                {
                    App.UserInfo = Barrel.Current.Get<TD.Api.Dtos.UserItem>(key: App.CacheUserInfoKey);
                    UserName = "Compte : " + App.UserInfo.Email + ".\n Utilisateur : " + App.UserInfo.FirstName;
                }

            }
            else
            {
                // lorsque le user n'est pas connecté on affiche les boutons de connexion et d'enregistrment
                HomePage = "Identification";
                UserNotLogged = true;
                UserLogged = false;
            }

        }
    }
}
