﻿using Common.Api.Dtos;
using FourPlaces.ServicePhoto;
using FourPlaces.RestApiServices;
using FourPlaces.Views.NewPlacePage;
using Storm.Mvvm;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class NewPlaceViewModel : ViewModelBase
    {
        private Image _image;
        private NewPlace _newPlace;
        private byte[] _fileByte;

        private bool _enableButtons;
        public bool EnableButtons
        {
            get => _enableButtons;
            set => SetProperty(ref _enableButtons, value);
        }

        private bool _displayDefaultCoord;
        public bool DisplayDefaultCoord
        {
            get => _displayDefaultCoord;
            set => SetProperty(ref _displayDefaultCoord, value);
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }
       
        private string placeName;
        public string PlaceName
        {
            get => placeName;
            set => SetProperty(ref placeName, value);
        }

        private string placeDescription;
        public string PlaceDescription
        {
            get => placeDescription;
            set => SetProperty(ref placeDescription, value);
        }

        private double placeLatitude;
        public double PlaceLatitude
        {
            get => placeLatitude;
            set => SetProperty(ref placeLatitude, value);
        }
        
        private double placeLongitude;
        public double PlaceLongitude
        {
            get => placeLongitude;
            set => SetProperty(ref placeLongitude, value);
        }

        public ICommand GoSaveCommand { get; }

        public ICommand GoPickPhotoCommand { get; }

        public ICommand GoTakePhotCommand { get; }

        public NewPlaceViewModel(NewPlace newPlace, Image photoImage)
        {
            _image = photoImage;
            _newPlace = newPlace;

            EnableButtons = true;
            DisplayDefaultCoord = false;

            GoTakePhotCommand = new Command(GoTakePhotoAction);
            GoPickPhotoCommand = new Command(GoPickPhotoAction);
            GoSaveCommand = new Command(GoSavePlaceAction);
        }

        private async void GoSavePlaceAction()
        {
            IsBusy = true;
            EnableButtons = false;

            // on essai de post l'image dans un premier temps 
            var res = await UploadImage();

            // l'image est uploadée
            if(res != null && res.IsSuccess == true)
            {
                // on a reussit a poster l'image donc res contient un ImageItem
                PostNewPlace(res);
            }
            EnableButtons = true;
            IsBusy = false;
        }

        private async Task<Response<ImageItem>> UploadImage()
        {
            // on essaie d'upload l'image
            if (_fileByte != null)
            {
                var res = await App.Rest_services.UploadImage(Constants.ImageUrl, _fileByte);

                if (res == null && !App.Rest_services.IsConnected())
                {
                    App.DisplayMessageService.InternetConnectionNeeded(_newPlace);
                }
                else
                {
                    // on a réussit à poster l'image
                    if (res != null && res.IsSuccess)
                    {
                        return res;
                    }
                    else
                    {
                        App.DisplayMessageService.DisplayMessage(_newPlace, "Erreur", "Impossible d'enregistrer l'image.", "OK");
                        EnableButtons = true;
                        await _newPlace.Navigation.PopAsync();
                    }
                }
                return null;
            }
            else
            {
                App.DisplayMessageService.DisplayMessage(_newPlace, "Erreur", "Veuillez selectionner une image avant d'essayer d'enregistrer le nouveau lieu SVP.", "OK");
                return null;
            }
            
        }

        private async void PostNewPlace(Response<ImageItem> response)
        {
            if (PlaceName != null && PlaceName.Length > 0 && PlaceDescription != null && PlaceDescription.Length > 0)
            {
                // on cree notre requete 
                CreatePlaceRequest request = new CreatePlaceRequest
                {
                    Description = PlaceDescription,
                    ImageId = response.Data.Id,
                    Latitude = PlaceLatitude,
                    Longitude = PlaceLongitude,
                    Title = PlaceName
                };

                var result = await App.Rest_services.PostResponse<Response>(Constants.PlacesUrl, request);

                if (result == null && !App.Rest_services.IsConnected())
                {
                    App.DisplayMessageService.InternetConnectionNeeded(_newPlace);
                }
                else
                {
                    if (result != null && result.IsSuccess) //  on a poster un nouveau lieu
                    {
                        App.DisplayMessageService.DisplayMessage(_newPlace, "Succes", "Nouvelle place enregistrée.", "OK");
                        await _newPlace.Navigation.PopToRootAsync();
                    }
                    else
                    {
                        App.DisplayMessageService.DisplayMessage(_newPlace, "Erreur", "Impossible d'enregistrer la saisie.", "OK");
                    }
                }
            }
            else App.DisplayMessageService.DisplayEmptyFormError(_newPlace);
            
        }
        
        private async void GoPickPhotoAction()
        {
            IsBusy = true;
            PhotoService service = new PhotoService();
            var file = await service.PickPhoto(_newPlace);
            if(file != null)
            {
                _fileByte = service.FileMediaToByteArray(file);
                _image.Source = service.GetImageSource(file);
            }
            IsBusy = false;
        }

        private async void GoTakePhotoAction()
        {
            IsBusy = true;
            PhotoService service = new PhotoService();
            var file = await service.TakePhoto(_newPlace);
            if (file != null)
            {
                _fileByte = service.FileMediaToByteArray(file);
                _image.Source = service.GetImageSource(file);
            }
            IsBusy = false;
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            IsBusy = true;
            // On recupère la position de l'utilisateur, et celle de Paris par défaut
            var userPosition = await App.LocationService.GetUserLocation();
            if(userPosition.Latitude == App.LocationService.Default_Latitude && userPosition.Longitude == App.LocationService.Default_Longitude)
            {
                DisplayDefaultCoord = true;
            }
            PlaceLatitude = userPosition.Latitude;
            PlaceLongitude = userPosition.Longitude;
            IsBusy = false;
        }
    }
}
