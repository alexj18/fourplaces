﻿using FourPlaces.Pages.RegistrationPage;
using FourPlaces.RestApiServices;
using MonkeyCache.FileStore;
using Storm.Mvvm;
using System;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class RegistrationViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private readonly Registration _registration;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        private bool _enableRegisterButton;
        public bool EnableRegisterButton
        {
            get => _enableRegisterButton;
            set => SetProperty(ref _enableRegisterButton, value);
        }

        public ICommand GoRegisterCommand
        {
            get;
        }
        

        public RegistrationViewModel(Registration registration)
        {
            GoRegisterCommand = new Command(GoAuthAction);
            _navigation = registration.Navigation;
            _registration = registration;
            EnableRegisterButton = true;
        }

        private async void GoAuthAction()
        {
            // on verifie que le formulaire est bien remplit
            if (FirstName != null && FirstName.Length > 0 && LastName != null && LastName.Length > 0 && Email != null && Email.Length > 0 && Password != null && Password.Length > 0)
            {
                EnableRegisterButton = false;
                IsBusy = true;
                RegisterRequest user = new RegisterRequest
                {
                    FirstName = FirstName,
                    LastName = LastName,
                    Email = Email,
                    Password = Password
                };

                // appel APi pour enregistrement d'un nouveau user
                var result = await App.Rest_services.PostResponse<LoginResult>(Constants.RegisterUrl, user);

                if (result == null && !App.Rest_services.IsConnected())
                {
                    App.DisplayMessageService.InternetConnectionNeeded(_registration);
                }
                else
                {
                    var msg = "";
                    if (result == null || result.IsSuccess == false)
                    {
                        //Cas où une erreur est survenue
                        msg = "Erreur lors de la création du compte.\n ";
                        if (result != null && result.ErrorMessage != null)
                        {
                            //Cas où une erreur est survenue mais que la response vient de l'API
                            msg += result.ErrorMessage;
                        }
                        App.DisplayMessageService.DisplayMessage(_registration, "Erreur", msg, "OK");
                    }
                    else
                    {
                        //Cas où tout c'est bien passé lors de la création du compte
                        msg = "Création de compte réussie.";
                        App.tokenInfo = result.Data;
                        // on met le token en cache
                        Barrel.Current.Add(key: App.CacheTokenKey, data: App.tokenInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));

                        // on recup les donnees du user
                        var userinfo = await App.Rest_services.GetResponse<UserItem>(Constants.UserInfosUrl);

                        if(userinfo != null)
                        {
                            App.UserInfo = new UserItem
                            {
                                Email = userinfo.Data.Email,
                                FirstName = userinfo.Data.FirstName
                            };
                            // on recup les donnees du user
                            Barrel.Current.Add(key: App.CacheUserInfoKey, data: App.UserInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));
                        }
                        
                        App.DisplayMessageService.DisplayMessage(_registration, "Succès", msg, "OK");
                        IsBusy = false;
                        EnableRegisterButton = true;
                        await _navigation.PopToRootAsync();
                    }
                }
                EnableRegisterButton = true;
                IsBusy = false;
            }
            else App.DisplayMessageService.DisplayEmptyFormError(_registration);
        }
    }
}
