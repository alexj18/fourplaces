﻿using Common.Api.Dtos;
using FourPlaces.Pages.ProfilePage;
using FourPlaces.RestApiServices;
using FourPlaces.ServicePhoto;
using Storm.Mvvm;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class ProfileViewModel : ViewModelBase
    {
        private UserItem user;
        private Profile _profile;
        private INavigation _navigation;
        private byte[] _fileByte;
        private bool pickedPhoto;

        // From Profile.xaml
        private Image _image;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private bool _displayUpdateProfile;
        public bool DisplayUpdateProfile
        {
            get => _displayUpdateProfile;
            set => SetProperty(ref _displayUpdateProfile, value);
        }

        private bool _displayUpdatePassword;
        public bool DisplayUpdatePassword
        {
            get => _displayUpdatePassword;
            set => SetProperty(ref _displayUpdatePassword, value);
        }

        private bool _enableButtons;
        public bool EnableButtons
        {
            get => _enableButtons;
            set => SetProperty(ref _enableButtons, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }
        
        private string _oldPass;
        public string OldPass
        {
            get => _oldPass;
            set => SetProperty(ref _oldPass, value);
        }

        private string _newPass;
        public string NewPass
        {
            get => _newPass;
            set => SetProperty(ref _newPass, value);
        }
        
        public ICommand GoModifCommand
        {
            get;
        }

        public ICommand GoModifPassCommand
        {
            get;
        }
        
       public ICommand GoPickPhotoCommand
       {
           get;
       }
        
        public ICommand GoDisplayUpdatePassCommand
        {
            get;
        }
        
        public ProfileViewModel(Profile profile,Image profile_image)
        {
            _profile = profile;
            _navigation = profile.Navigation;
            _image = profile_image;
            pickedPhoto = false;

            DisplayUpdatePassword = false;
            DisplayUpdateProfile = true;
            EnableButtons = true;
            
            GoModifCommand = new Command(GoModifAction);
            GoModifPassCommand = new Command(GoModifPassAction);
            GoPickPhotoCommand = new Command(GoPickPhotoAction);
            GoDisplayUpdatePassCommand = new Command(GoDisplayUpdatePassAction);
        }

        private void GoDisplayUpdatePassAction()
        {
            // On cache les bouttons qui ne sont pas lies à la modification du mot de passe
            DisplayUpdatePassword = true;
            DisplayUpdateProfile = false;
        }

        private async void GoPickPhotoAction()
        {
            IsBusy = true;
            PhotoService service = new PhotoService();
            var file = await service.PickPhoto(_profile);
            if(file != null)
            {
                _fileByte = service.FileMediaToByteArray(file);
                _image.Source = service.GetImageSource(file);
                pickedPhoto = true;
            }
            IsBusy = false;
        }

        // Permet de modifier le mot de passe d'un user
        private async void GoModifPassAction()
        {
            // on modifie que si les mots de passe ont un champs non vide
            if(OldPass != null && NewPass != null && OldPass.Length > 0 && NewPass.Length > 0)
            {
                UpdatePasswordRequest updateRequest = new UpdatePasswordRequest
                {
                    OldPassword = OldPass,
                    NewPassword = NewPass
                };
                // on empeche l'utilisateur d'appuyer de nouveau sur un boutton avant la fin de l'éxécution de la méthode
                EnableButtons = false;
                IsBusy = true; // affiche un loader 
                var result = await App.Rest_services.PatchResponse<Response>(Constants.EditPasswordUrl, updateRequest);
                if( result != null && !App.Rest_services.IsConnected())
                {
                    await _profile.DisplayAlert("Alert", "Veuillez activer la connexion internet SVP.", "OK");
                }
                else
                {
                    if (result != null && result.IsSuccess)
                    {
                        await _profile.DisplayAlert("Alert", "Mot de passe modifié.", "OK");
                        EnableButtons = true;
                        IsBusy = false;
                        await _navigation.PopAsync();
                    }
                    else
                    {
                        await _profile.DisplayAlert("Alert", "Impossible de modifier votre mot de passe", "OK");
                    }
                }
                EnableButtons = true;
                IsBusy = false;
            }
            else
            {
                await _profile.DisplayAlert("Alert", "Veuillez remplir les deux champs svp.", "OK");
            }
        }

        // Permet de modifier Le nom et prenom d'un user
        private async void GoModifAction()
        {
            Response<ImageItem> res = null;
            // on essai de post l'image dans un premier temps 
            if (_fileByte != null)
            {
                // !=null si on a cliqué sur le boutton "Image de profile"
                res = await UploadImage();
            }
            
            UpdateProfileRequest updateRequest;
            // l'image est uploadée
            if (res != null)
            {
                // on a reussit a poster l'image donc res contient un ImageItem
                updateRequest = new UpdateProfileRequest
                {
                    FirstName = FirstName,
                    LastName = LastName,
                    ImageId = res.Data.Id
                };
            }
            else
            {
                // on modifie juste le le nom et le prenom
                updateRequest = new UpdateProfileRequest
                {
                    FirstName = FirstName,
                    LastName = LastName,
                };
            }
            EnableButtons = false;
           IsBusy = true;
           var result = await App.Rest_services.PatchResponse<UserItem>(Constants.UserInfosUrl, updateRequest);

            if (result != null && !App.Rest_services.IsConnected())
            {
                await _profile.DisplayAlert("Alert", "Veuillez activer la connexion internet SVP.", "OK");
            }
            else
            {
                if (result != null && result.IsSuccess)
                {
                    await _profile.DisplayAlert("Alert", "Modification enregistrée.\n Nouveau prenom:" + FirstName + ", \n nouveau nom" + LastName + ".", "OK");
                    IsBusy = false;
                    EnableButtons = true;
                    await _navigation.PopAsync();
                }
                else
                {
                    await _profile.DisplayAlert("Alert", "Impossible d'enregistrer les modifications au profile.", "OK");
                }
            }
            IsBusy = false;
            EnableButtons = true;
        }

        private async Task<Response<ImageItem>> UploadImage()
        {
            // on essaie d'upload l'image
            var res = await App.Rest_services.UploadImage(Constants.ImageUrl, _fileByte);

            if (res == null && !App.Rest_services.IsConnected())
            {
                var msg = "Veuillez activer la connexion internet SVP.";
                await _profile.DisplayAlert("Alert", msg, "OK");
            }
            else
            {
                // on a réussit à poster l'image
                if (res != null && res.IsSuccess)
                {
                    return res;
                }
                else
                {
                    await _profile.DisplayAlert("Erreur", "Impossible d'enregistrer l'image.", "OK");
                    await _profile.Navigation.PopAsync();
                }
            }
            return null;
        }


        public override async Task OnResume()
        {
            await base.OnResume();
            IsBusy = true;
            var result = await App.Rest_services.GetResponse<UserItem>(Constants.UserInfosUrl);
            if (result != null && !App.Rest_services.IsConnected())
            {
                await _profile.DisplayAlert("Alert", "Veuillez activer la connexion internet SVP.", "OK");
            }
            else
            {
                if (result.Data != null && result.IsSuccess)
                {
                    user = result.Data;
                    Email = user.Email;
                    LastName = user.LastName;
                    FirstName = user.FirstName;
                    if(!pickedPhoto)
                        _image.Source = user.Image;
                }
                else
                {
                    await _profile.DisplayAlert("Alert", "Impossible de charger les données de l'utilisateur.", "OK");
                    await _navigation.PopAsync();
                }
            }
            IsBusy = false;
        }
    }
}
