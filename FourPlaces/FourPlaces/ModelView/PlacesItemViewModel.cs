﻿using FourPlaces.RestApiServices;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace FourPlaces.ModelView
{
    class PlacesItemViewModel : ViewModelBase
    {
        private Views.PlaceItemPage.PlaceItem _placeItem;
        private INavigation _navigation;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
        
        private ObservableCollection<PlaceItemSummary> place;
        public ObservableCollection<PlaceItemSummary> Places
        {
            get => place;
            set => SetProperty(ref place, value);
        }
        
        public PlacesItemViewModel(Views.PlaceItemPage.PlaceItem placeItem)
        {
            _placeItem = placeItem;
            _navigation = placeItem.Navigation;
            Title = "Points d'intérêts proches de vous";
            Places = new ObservableCollection<PlaceItemSummary>();
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            App.LocationService.RefreshUserPosition();
            if (App.LocationService.IsDefaultLocation())
                App.DisplayMessageService.DisplayDefaultLocation(_placeItem);
            IsBusy = true;
            // On recupere la liste des lieux disponibles dans l'API
            var res = await App.Rest_services.GetResponse<List<PlaceItemSummary>>(Constants.PlacesUrl);
            if (res == null && !App.Rest_services.IsConnected())
            {
                App.DisplayMessageService.InternetConnectionNeeded(_placeItem);
            }
            else
            {
                if (res == null || res.IsSuccess == false)
                {
                    string msg = "Impossible d'afficher les points d'intêrets se situant près de vous.\n";
                    if (res != null && res.ErrorMessage != null)
                    {
                        msg += res.ErrorMessage;
                    }
                    App.DisplayMessageService.DisplayMessage(_placeItem, "Alert", msg, "OK");
                    IsBusy = false;
                    await _navigation.PopToRootAsync();
                }
                else
                {
                    //on clear Places avant de l'update 
                    Places.Clear();
                    List<PlaceItemSummary> tmp = new List<PlaceItemSummary>();
                    foreach (var elt in res.Data)
                    {
                        elt.SmallDescription();
                        var positionFormUser = App.LocationService.CalculateDistanceAsync(new Position(elt.Latitude,elt.Longitude));
                        elt.SetDistanceFromUser(positionFormUser);
                        tmp.Add(elt);
                    }

                    Console.WriteLine("before sorting");
                    tmp.Sort();
                    Console.WriteLine("after sorting");
                    foreach (var elt in tmp)
                    {
                        Places.Add(elt);
                    }
                }
            }
            IsBusy = false;

        }

    }
}

