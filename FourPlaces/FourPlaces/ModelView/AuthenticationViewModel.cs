﻿using Common.Api.Dtos;
using FourPlaces.Pages.AuthenticationPage;
using FourPlaces.RestApiServices;
using MonkeyCache.FileStore;
using Storm.Mvvm;
using System;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ModelView
{
    class AuthenticationViewModel : ViewModelBase
    {
        private INavigation _navigation;
        private readonly Authentication _auth;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        private bool _enableSignInButton;
        public bool EnableSignInButton
        {
            get => _enableSignInButton;
            set => SetProperty(ref _enableSignInButton, value);
        }
        
        public ICommand GoAuthCommand
        {
            get;
        }

        public AuthenticationViewModel(Authentication auth)
        {
            GoAuthCommand = new Command(GoAuthAction);
            _navigation = auth.Navigation;
            _auth = auth;
            EnableSignInButton = true;
        }

        private async void GoAuthAction()
        {
            if (Email != null && Email.Length > 0 && Password != null && Password.Length > 0)
            {
                EnableSignInButton = false;
                IsBusy = true;

                LoginRequest login = new LoginRequest
                {
                    Email = Email,
                    Password = Password
                };

                // Requete API pour s'authentifier
                var result = await App.Rest_services.PostResponse<LoginResult>(Constants.LoginUrl, login);

                // l'utilisateur n'est pas conecté à internet 
                if (result == null && !App.Rest_services.IsConnected())
                {
                    App.DisplayMessageService.InternetConnectionNeeded(_auth);
                }
                else
                {
                    string msg = "";
                    if (result == null || result.IsSuccess == false)
                    {
                        msg = "Connexion impossible.\n";
                        if (result != null && result.ErrorMessage != null)
                        {
                            msg += result.ErrorMessage;
                        }
                        App.DisplayMessageService.DisplayMessage(_auth, "Erreur", msg, "OK");
                    }
                    else
                    {
                        //msg = "Vous êtes connecté.";
                        // On recupère dans res.Data le token 
                        App.tokenInfo = result.Data;
                        Barrel.Current.Add(key: App.CacheTokenKey, data: App.tokenInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));
                        
                        // on recup les donnees du user
                        var userinfo = await App.Rest_services.GetResponse<UserItem>(Constants.UserInfosUrl);

                        if (userinfo != null)
                        {
                            App.UserInfo = new UserItem
                            {
                                Email = userinfo.Data.Email,
                                FirstName = userinfo.Data.FirstName
                            };
                            // on recup les donnees du user
                            Barrel.Current.Add(key: App.CacheUserInfoKey, data: App.UserInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));
                        }

                        Barrel.Current.Add(key: App.CacheUserInfoKey, data: App.UserInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));
                        //App.DisplayMessageService.DisplayMessage(_auth, "Succès", msg, "OK");
                        EnableSignInButton = true;
                        IsBusy = false;
                        await _navigation.PopToRootAsync();
                    }
                }

                EnableSignInButton = true;
                IsBusy = false;
            }
            else App.DisplayMessageService.DisplayEmptyFormError(_auth);
        }   
    }
}
