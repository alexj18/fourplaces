﻿using FourPlaces.RestApiServices;
using FourPlaces.ServiceDisplayMessage;
using MonkeyCache.FileStore;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FourPlaces.SercviceLocation;
using Xamarin.Essentials;
using System.Collections.Generic;
using Common.Api.Dtos;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FourPlaces
{
    public partial class App : Application
    {
        // contient les infos du token si le user est connecté
        public static LoginResult tokenInfo;

        public static readonly string CacheTokenKey = "Token";

        public static readonly int CacheDuration = 1;

        public static UserItem UserInfo;

        public static readonly string CacheUserInfoKey = "UserInfo";
        
        //Singleton permettant de communiquer avec l'API rest (https://td-api.julienmialon.com)
        private static RestApi rest_services;
        public static RestApi Rest_services {
            get {
                if(rest_services == null)
                {
                    rest_services = new RestApi();
                }
                return rest_services;
            }
        }

        //Singleton permettant de recuperer les coordonnées de l'utilisateur
        private static LocationService _locationService;
        public static LocationService LocationService
        {
            get
            {
                if (_locationService == null)
                {
                    _locationService = new LocationService();
                }
                return _locationService;
            }
        }

        //Singleton permettant d'afficher des messages commun à toute l'application
        private static DisplayMessageService _messageService;
        public static DisplayMessageService DisplayMessageService
        {
            get
            {
                if (_messageService == null)
                {
                    _messageService = new DisplayMessageService();
                }
                return _messageService;
            }
        }

        public App()
        {
            InitializeComponent();
            Barrel.ApplicationId = "FourPlaces";
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            

            // on supprime du cache tout ce qui n'est plus valide
            if(Barrel.Current != null)
            {
                Barrel.Current.EmptyExpired();
                // on regarde si le token est en cache 
                tokenInfo = Barrel.Current.Get<LoginResult>(key: CacheTokenKey);


                // On supprime la liste des lieux du cache a chaque demarrage de l'appli,
                // car l'utilisateur a peut être changé de position depuis que la liste a été mise en cache.
                // elle sera donc rechargée lors de la première visite sur la page correspondante
                string ListPlacesKey = "GEThttps://td-api.julienmialon.com/places";
                if(Barrel.Current.Get<Response<List<PlaceItemSummary>>>(key : ListPlacesKey) != null)
                {
                    string[] keyArray = new string[1];
                    keyArray[0] = ListPlacesKey;
                    Barrel.Current.Empty(keyArray);
                }
                    
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
