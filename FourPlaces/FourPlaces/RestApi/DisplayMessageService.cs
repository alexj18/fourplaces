﻿using Storm.Mvvm.Forms;

namespace FourPlaces.ServiceDisplayMessage
{
    public class DisplayMessageService
    {
        public async void InternetConnectionNeeded(BaseContentPage page)
        {
            await page.DisplayAlert("Alert", "Veuillez activer la connexion internet.", "OK");
        }

        public async void InternetConnectionNotActivated(BaseContentPage page)
        {
            await page.DisplayAlert("Alert", "Pour utiliser pleinement l'application, veuillez activer la connexion internet SVP.", "OK");
        }

        public async void DisplayEnableGeolocation(BaseContentPage page)
        {
            await page.DisplayAlert("Alert", "Pensez à activer la geolocalisation si ce n'est pas déjà fait. Autrement, les coordonnées de Paris (latitude : 48.856614, longitude : 2.3522219) seront utilisées par défaut.", "OK");
        }

        public async void DisplayEmptyFormError(BaseContentPage page)
        {
            await page.DisplayAlert("Erreur", "Veuillez remplir tous les champs correctement SVP.", "OK");
        }

        public async void DisplayDefaultLocation(BaseContentPage page)
        {
            await page.DisplayAlert("Alert", "Les coordonnées par défaut sont utilisées. Veuillez activer la géolocalisation et rédemarrer l'application si vous voulez mettre à jour votre position.", "OK");
        }

        public async void DisplayMessage(BaseContentPage page,string title, string msg, string msgButton)
        {
            await page.DisplayAlert(title, msg, msgButton);
        }

        

    }
}
