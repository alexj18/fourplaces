﻿using Common.Api.Dtos;
using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TD.Api.Dtos;

namespace FourPlaces.RestApiServices
{
    public class RestApi 
    {
           
        public RestApi() {}

        // Permet de savoir si l'utilisateur est connecté au réseau
        public bool IsConnected()
        {
            return CrossConnectivity.Current.IsConnected;

        }

        // Mise a jour du token
        private async Task<bool> RefreshToken()
        {
            RefreshRequest request = new RefreshRequest
            {
                RefreshToken = App.tokenInfo.RefreshToken
            };
            var res = await PostResponse<LoginResult>(Constants.RefreshTokenUrl, request);
            if (res != null && res.IsSuccess)
            {
                App.tokenInfo = res.Data;
                Barrel.Current.Add(key: App.CacheTokenKey, data: App.tokenInfo, expireIn: TimeSpan.FromDays(App.CacheDuration));
                return true;
            }
            return false;
        }

        public async Task<Response<T>> PostResponse<T>(string url, object data) where T : class
        {
            // on met les data sous form JSON
            var JsonData = JsonConvert.SerializeObject(data);
            HttpContent content = new StringContent(JsonData, Encoding.UTF8, "application/json");

            var result = await ApiRequest<T>(HttpMethod.Post, url, content);

            // On test si le token a besoin d'être refresh
            if (result != null && result.ErrorMessage == "expired token")
            {
                var refreshSuccess = await RefreshToken();
                if (refreshSuccess)
                {
                    result = await ApiRequest<T>(HttpMethod.Post, url, content);
                }
            }
            return result;
        }

        public async Task<Response<T>> GetResponse<T>(string url) where T : class
        {
            Response<T> result = null;
            // Avant de faire une requete à l'API on test si les data sont en cache
            var key = "GET" + url;

            if (Barrel.Current != null && !Barrel.Current.IsExpired(key: key))
            {
                result = Barrel.Current.Get<Response<T>>(key: key);
            }
            if (result == null)
                result = await ApiRequest<T>(HttpMethod.Get, url);

            // On test si le token a besoin d'être refresh
            if (result != null && result.ErrorMessage == "expired token")
            {
                var refreshSuccess = await RefreshToken();
                if (refreshSuccess)
                {
                    result = await ApiRequest<T>(HttpMethod.Get, url);
                }
            }
            return result;
        }

        public async Task<Response<T>> PatchResponse<T>(string url, object data) where T : class
        {
            var method = new HttpMethod("PATCH");
            var JsonData = JsonConvert.SerializeObject(data);
            var request = new HttpRequestMessage(method, url)
            {
                Content = new StringContent(JsonData, Encoding.UTF8, "application/json")
            };
            var result = await ApiRequest<T>(method, url, null, request);

            // On test si le token a besoin d'être refresh
            if (result != null && result.ErrorMessage == "expired token")
            {
                var refreshSuccess = await RefreshToken();
                if (refreshSuccess)
                {
                    result = await ApiRequest<T>(method, url, null, request);
                }
            }
            return result;
        }

        // Methode qui gere les requetes à l'API et les erreurs qui peuvent survenir
        // T indique ici les Data qui seront contenus dans la réponse
        private async Task<Response<T>> ApiRequest<T>(HttpMethod sendMethod, string url, HttpContent content = null, HttpRequestMessage request = null) where T: class
        {
            // on verifie la connexion internet de l'utilisateur
            if (IsConnected())
            {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        //On set le token access dans le header si il existe
                        if (App.tokenInfo != null)
                        {
                            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(App.tokenInfo.TokenType, App.tokenInfo.AccessToken);
                        }
                        HttpResponseMessage response = new HttpResponseMessage();
                        if (sendMethod == HttpMethod.Get)
                        {
                            response = await client.GetAsync(url);
                        }
                        else if (sendMethod == HttpMethod.Post)
                        {
                            if (content != null)
                                response = await client.PostAsync(url,content);
                            else
                                // pour une methode POST dans cette application, content ne doit pas être vide
                                return null;
                        }
                        else if (sendMethod.Method == "PATCH")
                        {
                            if(request != null)
                                response = await client.SendAsync(request);
                            else
                                // pour une methode PATCH dans cette application, request ne doit pas être vide
                                return null;
                        }
                        if (response != null)
                        {
                            var result = await GetObjectFromResponse<T>(response,sendMethod,url);
                            return result;
                        }
                        else
                            return null;
                    }
                    catch (HttpRequestException e)
                    {
                        Console.WriteLine("\nException Caught! Erreur Http");
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine("\nMessage : {0} ", e.Message);
                    }
                    
                }
            }
            else
            {
                // On essai de recuperer les data dans le cache
                var key = sendMethod.Method + url;
                if (!Barrel.Current.IsExpired(key: key))
                {
                    return Barrel.Current.Get<Response<T>>(key: key);
                }
            }
            // return null si l'appreil n'est pas connecté à internet
            // return null si une erreur est survenue
            return null;
        }

        private async Task<Response<T>> GetObjectFromResponse<T>(HttpResponseMessage response, HttpMethod sendMethod, string url) where T: class 
        {
            try
            {
                // on recupere la reponse de l'api
                var responseBody = await response.Content.ReadAsStringAsync();
                // on la convertit sous forme de Response avec T comme data
                var result = JsonConvert.DeserializeObject<Response<T>>(responseBody);
                var key = sendMethod.Method + url;
                // Ajout en cache des data recuperer par methode GET
                if( result != null && sendMethod == HttpMethod.Get)
                {
                    Barrel.Current.Add(key: key, data: result, expireIn: TimeSpan.FromDays(App.CacheDuration));
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("\nMessage : {0} ", e.Message);
            }
            return null;
        }
        
        public async Task<Response<ImageItem>> UploadImage(string url, byte[] imageData)
        {
            if (IsConnected())
            {
                HttpClient client = new HttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);
                if (App.tokenInfo != null)
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue(App.tokenInfo.TokenType, App.tokenInfo.AccessToken);
                }

                MultipartFormDataContent requestContent = new MultipartFormDataContent();
                var imageContent = new ByteArrayContent(imageData);
                imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

                // Le deuxième paramètre doit absolument être "file" ici sinon ça ne fonctionnera pas
                requestContent.Add(imageContent, "file", "file.jpg");
                request.Content = requestContent;
                try
                {
                    HttpResponseMessage response = await client.SendAsync(request);
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response<ImageItem>>(responseBody);
                    return result;
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("\nException Caught! Erreur Http");
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine("\nMessage : {0} ", e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nException Caught! Impossible de désérialisé la réponse");
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine("\nMessage : {0} ", e.Message);
                }
                
            }
            return null;
        }
        
        //Api Places de Google
        /*public static async void nearbyplaces()
        {
            getLocation();
            // permet de recuperer le slieux proches de la localisation du user
            //&type=locality
            string url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + CommServeur._latitude + ","+CommServeur._longitude + "&radius=1500&language=fr&type=point_of_interest&key=AIzaSyDbwkCUpebBAUS_khus2QgBIYEPPzgViEQ";
            //var response = await getRequest(url);
            //Console.WriteLine(response);
            displayCoords();
        }*/
    }
}
