﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace FourPlaces.SercviceLocation
{
    public class LocationService
    {

        public double Default_Latitude { get; } = 48.856614;
        public double Default_Longitude { get; } = 2.3522219;
        private Location userLocation;

        // Permet de recuperer la position du user ou a defaut renvoi celle de Paris
        public async Task<Position> GetUserLocation()
        {
            if (App.Rest_services.IsConnected())
            {
                var location = await UserLocation();

                if (location != null)
                    return new Position(location.Latitude,location.Longitude);

                else return new Position(Default_Latitude, Default_Longitude);
            }
            else return new Position(Default_Latitude, Default_Longitude);
        }

        // Méthode qui retourne la distance entre la postion du user et la position passée en paramètre (en mètres )
        public double CalculateDistanceAsync(Position position)
        {
            if(userLocation == null)
            {
                RefreshUserPosition();
            }
            var distance = userLocation.CalculateDistance(position.Latitude, position.Longitude, DistanceUnits.Kilometers);
            return distance;
        }

        public async void RefreshUserPosition()
        {
            userLocation = await UserLocation();
        }

        public bool IsDefaultLocation()
        {
            return (userLocation.Latitude == Default_Latitude && userLocation.Longitude == Default_Longitude);
        }

        private async Task<Location> UserLocation()
        {
            try
            {
                var location = await Geolocation.GetLocationAsync();

                if (location != null)
                    return location;
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
            }
            return new Location(Default_Latitude, Default_Longitude);
        }
    }
}
