﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Pages.MapPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Map : ContentPage
	{
		public Map ()
		{
			InitializeComponent ();
            SetUpUserLocation();
        }

        private async void SetUpUserLocation()
        {
            var user_position = await App.LocationService.GetUserLocation();
            Position pos = new Position(user_position.Latitude, user_position.Longitude);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(
                new Position(pos.Latitude, pos.Longitude),
                Distance.FromKilometers(1)
                ));
        }
    }
}