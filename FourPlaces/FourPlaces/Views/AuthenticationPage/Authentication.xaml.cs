﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Pages.AuthenticationPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Authentication : BaseContentPage
	{
        public Authentication()
        {
            InitializeComponent();
            BindingContext = new AuthenticationViewModel(this);
        }
	}
}