﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Pages.RegistrationPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registration : BaseContentPage
	{
		public Registration ()
		{
			InitializeComponent ();
            BindingContext = new RegistrationViewModel(this);
            
        }
	}
}