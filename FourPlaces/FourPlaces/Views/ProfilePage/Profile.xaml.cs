﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Pages.ProfilePage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Profile : BaseContentPage
	{
		public Profile ()
		{
			InitializeComponent ();
            BindingContext = new ProfileViewModel(this,profile_image);
		}
        
    }
}