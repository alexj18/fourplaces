﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;

namespace FourPlaces
{
    public partial class MainPage : BaseContentPage
    {
        public MainPage()
        {
            InitializeComponent();            
            BindingContext = new MainViewModel(this,Navigation);
        }

        
    }
}
