﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;
using TD.Api.Dtos;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Views.PlaceMapPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlaceMap : BaseContentPage
	{

        public PlaceMap()
        {
            InitializeComponent();
        }

        public PlaceMap (PlaceItemSummary item)
		{
			InitializeComponent ();
            BindingContext = new PlaceMapViewModel(this,item);

            // ajoute un pin sur la map aux coordonnées de l'item selecetionné
            var position = new Position(item.Latitude, item.Longitude); // Latitude, Longitude
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = item.Title
            };

            map.Pins.Add(pin);

            // deplace la map aux coordonnées de l'item
            Position pos = new Position(item.Latitude, item.Longitude);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(
                new Position(pos.Latitude, pos.Longitude),
                Distance.FromKilometers(1)
                ));
        }

    }
}
