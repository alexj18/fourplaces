﻿using FourPlaces.ModelView;
using Storm.Mvvm.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Views.NewPlacePage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewPlace : BaseContentPage
	{
		public NewPlace ()
		{
			InitializeComponent ();
            BindingContext = new NewPlaceViewModel(this,PhotoImage);
		}


	}
}