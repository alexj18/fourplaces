﻿using FourPlaces.ModelView;
using FourPlaces.Views.PlaceMapPage;
using Storm.Mvvm.Forms;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Views.PlaceItemPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlaceItem : BaseContentPage
	{

        public PlaceItem ()
		{
			InitializeComponent ();
            BindingContext = new PlacesItemViewModel(this);
        }

        //Methode pour afficher le detail d'un lieu
        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            
            // On envoie dans la nouvelle l'le lieu sur lequel on a tapé
            PlaceMap place = new PlaceMap((PlaceItemSummary)e.Item);
            await Navigation.PushAsync(place);

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }

        
    }
}