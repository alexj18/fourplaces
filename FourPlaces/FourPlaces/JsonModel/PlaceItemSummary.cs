using System;
using FourPlaces.RestApiServices;
using Newtonsoft.Json;

namespace TD.Api.Dtos
{
	public class PlaceItemSummary : IEquatable<PlaceItemSummary>, IComparable<PlaceItemSummary>
    {
		[JsonProperty("id")]
		public int Id { get; set; }
		
		[JsonProperty("title")]
		public string Title { get; set; }
		
		[JsonProperty("description")]
		public string Description { get; set; }
		
		[JsonProperty("image_id")]
		public int ImageId { get; set; }
		
		[JsonProperty("latitude")]
		public double Latitude { get; set; }
		
		[JsonProperty("longitude")]
		public double Longitude { get; set; }

        public string ImageUrl
        {
            get => Constants.BaseUrl + "/images/" + ImageId;
        }

        public string SDescription { get; set; }
        
        public void SmallDescription()
        {
            if(Description != null && Description.Length >=12)
            {
                SDescription = Description.Substring(0, 12) + "...";
            }
            
        }

        public double DistanceFromUser { get; private set; }

        internal void SetDistanceFromUser(double positionFormUser)
        {
            DistanceFromUser = positionFormUser;
        }

        // On veut pouvoir trier les lieux en fonctions de leur distances par rapport � l'utilisateur

        public bool Equals(PlaceItemSummary other)
        {
            if (other == null) return false;
            return this.DistanceFromUser.Equals(other.DistanceFromUser);
        }

        public int CompareTo(PlaceItemSummary other)
        {
            // A null value means that this object is greater.
            if (other == null)
                return 1;

            else
                return this.DistanceFromUser.CompareTo(other.DistanceFromUser);
        }
    }
}